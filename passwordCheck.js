const passwordService = require('./passwordService');

module.exports = (req, res, next) => {
  if (!passwordService.check(req.params.filename, req.query.password)) {
    return res.status(400).json({message: "Access denied, wrong password"});
  }
  next();
}
