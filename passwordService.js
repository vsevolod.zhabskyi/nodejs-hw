const fs = require("fs");

class PasswordService {
  constructor() {
    this.storageJson = 'passwordStorage.json';
    this.storage = [];

    fs.readFile(this.storageJson, (err, data) => {
      this.storage = JSON.parse(data);
    })
  }

  add(filename, password) {
    this.storage.push({filename, password});
    this.updateStorage();
  }

  check(filename, password) {
    const pair = this.storage.find(pair => pair.filename === filename);

    if (pair && pair.password !== password) {
      return false;
    }
    return true;
  }

  update(oldFilename, newFilename) {
    const pair = this.storage.find(pair => pair.filename === oldFilename);
    pair.filename = newFilename;
    this.updateStorage();
  }

  delete(filename) {
    const index = this.storage.findIndex(pair => pair.filename === filename);
    this.storage.splice(index, 1);
    this.updateStorage();
  }

  updateStorage() {
    fs.writeFile(this.storageJson, JSON.stringify(this.storage), (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
}

module.exports = new PasswordService();
