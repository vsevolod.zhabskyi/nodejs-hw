const express = require('express');
const router = require('./router');

const app = express();
app.use(express.json());
app.use('/', (req, res, next) => {
  console.log(req.method, req.url);
  next();
})
app.use('/api', router)

app.listen(8080);
