const passwordCheck = require('./passwordCheck');
const fileController = require('./fileController');
const router = require('express').Router({mergeParams: true});

router.get('/files', fileController.getAll);
router.get('/files/:filename', passwordCheck, fileController.getOne);
router.post('/files', fileController.create);
router.delete('/files/:filename', passwordCheck, fileController.delete);
router.put('/files/:filename', passwordCheck, fileController.update);

module.exports = router
