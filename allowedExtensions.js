const allowedExtensions = [
  'txt',
  'log',
  'json',
  'yaml',
  'xml',
  'js'
]

module.exports = allowedExtensions;
