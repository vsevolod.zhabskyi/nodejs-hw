const fs = require("fs");
const allowedExtensions = require('./allowedExtensions');
const passwordService = require('./passwordService');

const DIR_NAME = 'storage';

function getFileExtension(filename) {
  return filename.split('.').pop();
}

class FileController {

  async getOne(req, res) {
    try {
      if (!req.params.filename) {
        return res.status(400).json({message: "Please specify 'filename' parameter"});
      }
      const {filename} = req.params;

      if (!fs.existsSync(DIR_NAME)) {
        return res.status(400).json({message: `No file with name ${filename} found`});
      }
      const data = fs.readdirSync(DIR_NAME);
      if (!data || !data.includes(filename)) {
        return res.status(400).json({message: `No file with name ${filename} found`});
      }

      const uploadedDate = fs.statSync(`${DIR_NAME}/${filename}`).birthtime;
      const content = fs.readFileSync(`${DIR_NAME}/${filename}`, 'utf-8');
      const extension = getFileExtension(filename);

      return res.status(200).json({
        message: "Success",
        filename,
        content,
        extension,
        uploadedDate
      })
    } catch(e) {
      console.log(e)
      return res.status(500).json({message: "Server error"});
    }
  }

  async getAll(req, res) {
    try {
      fs.readdir(DIR_NAME, (err, data) => {
        if (!data || data.length === 0) {
          return res.status(400).json({message: "No files stored"});
        }
        return res.status(200).json({
          message: "Success",
          files: data
        })
      });
    } catch {
      return res.status(500).json({message: "Server error"});
    }
  }

  async create(req, res) {
    try {
      if (!req.body.filename) {
        return res.status(400).json({message: "Please specify 'filename' field"});
      }
      if (req.body.content === undefined) {
        return res.status(400).json({message: "Please specify 'content' field"});
      }

      const {filename, content} = req.body;

      const extension = getFileExtension(filename);

      if (!extension) {
        return res.status(400).json({message: "Please specify extension of the file"});
      }
      if (!allowedExtensions.includes(extension)) {
        return res.status(400).json({message: "Specified extension is not allowed"});
      }

      fs.mkdirSync(DIR_NAME, { recursive: true });
      const data = fs.readdirSync(DIR_NAME);
      if (data.includes(filename)) {
        return res.status(400).json({message: `File ${filename} already exists`});
      }

      if (req.query.password) {
        const {password} = req.query;
        passwordService.add(filename, password)
      }

      fs.writeFile(`${DIR_NAME}/${filename}`, content, () => {
        return res.status(200).json({message: "File created successfully"});
      });

    } catch(e) {
      console.log(e)
      return res.status(500).json({message: "Server error"});
    }
  }

  async update(req, res) {
    try {
      if (!req.params.filename) {
        return res.status(400).json({message: "Please specify 'filename' parameter"});
      }
      const {filename} = req.params;

      if (!req.body.filename) {
        return res.status(400).json({message: "Please specify 'filename' field"});
      }
      if (!req.body.content === undefined) {
        return res.status(400).json({message: "Please specify 'content' field"});
      }

      if (!fs.existsSync(DIR_NAME)) {
        return res.status(400).json({message: `No file with name ${filename} found`});
      }
      const data = fs.readdirSync(DIR_NAME);
      if (!data.includes(filename)) {
        return res.status(400).json({message: `No file with name ${filename} found`});
      }

      const newFilename = req.body.filename;
      const {content} = req.body;

      const extension = getFileExtension(newFilename);

      if (!extension) {
        return res.status(400).json({message: "Please specify extension of the file"});
      }
      if (!allowedExtensions.includes(extension)) {
        return res.status(400).json({message: "Specified extension is not allowed"});
      }

      if (data.includes(newFilename) && newFilename !== filename) {
        return res.status(400).json({message: `File ${newFilename} already exists`});
      }

      fs.renameSync(`${DIR_NAME}/${filename}`, `${DIR_NAME}/${newFilename}`);
      if (req.query.password) {
        passwordService.update(filename, newFilename);
      }
      fs.writeFile(`${DIR_NAME}/${newFilename}`, content, () => {
        return res.status(200).json({message: "File updated successfully"});
      });

    } catch (e) {
      console.log(e)
      return res.status(500).json({message: "Server error"});
    }

  }

  async delete(req, res) {
    try {
      if (!req.params.filename) {
        return res.status(400).json({message: "Please specify 'filename' field"});
      }

      const {filename} = req.params;

      if (!fs.existsSync(DIR_NAME)) {
        return res.status(400).json({message: `No file with name ${filename} found`});
      }
      const data = fs.readdirSync(DIR_NAME);
      if (!data.includes(filename)) {
        return res.status(400).json({message: `No file with name ${filename} found`});
      }

      if (req.query.password) {
        passwordService.delete(filename);
      }
      fs.unlink(`${DIR_NAME}/${filename}`, () => {
        return res.status(200).json({message: `File ${filename} was successfully deleted`});
      });

    } catch {
      return res.status(500).json({message: "Server error"});
    }
  }
}

module.exports = new FileController();
